var express = require("express");
var bodyParser = require("body-parser");
var app = express();
 
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/api', express.static(__dirname + '/client-api'));
 
var routes = require("./routes/routes.js")(app);
 
var server = app.listen(3000, function () {
    var host = server.address().address;
    var port = server.address().port;

    console.log("Listening on port http://%s:%s...", host, port);
});