(function(){
    ___ = function (d) {
        console.log(d);
    }

    Tracker = function () {

        var pathname = window.location.pathname;
        var search   = window.location.search;

        return {
            save : function ( data ) {
                if (typeof data === 'undefined') return this;

                var currentHistory = this.__history();

                if (typeof data.product !== 'undefined' ||
                    typeof data.category !== 'undefined' ||
                    typeof data.pageType !== 'undefined' ||
                    typeof data.categoryTree !== 'undefined') {
                    currentHistory.push( 
                        { 
                            product      : data.product,
                            category     : data.category,
                            categoryTree : data.categoryTree,
                            pageType     : data.pageType,
                            url          : pathname + search
                        }
                    );
                }

                localStorage.customerHistory = JSON.stringify(currentHistory);
                return this;
            },
            clear : function () {
                var currentHistory  = this.__history();
                var historyIterator = currentHistory.length;

                while (historyIterator--) {
                    if (typeof currentHistory[historyIterator].product === 'undefined' &&
                        typeof currentHistory[historyIterator].category === 'undefined' &&
                        typeof currentHistory[historyIterator].pageType === 'undefined' &&
                        typeof currentHistory[historyIterator].categoryTree === 'undefined') {
                        currentHistory.splice(historyIterator, 1);
                    }
                }

                localStorage.customerHistory = JSON.stringify(currentHistory);
                return this;
            },
            check : function () {
                this.clear();
                if (this.__verifyHistory()) {

                }
            },
            __verifyHistory : function () {
                var currentHistory = this.__history();
                
                for (var k in currentHistory) {
                    
                }
            },
            __history : function () {
                return (typeof localStorage.customerHistory !== 'undefined') ? JSON.parse(localStorage.customerHistory) : [];
            }
        }
    }
})();



Tracker().save();