var fs = require('fs');

var appRouter = function (app) {

    app.get('/', function (req, res) {
        fs.readFile( __dirname + "/../demo/index.html", 'utf8', function (err, data) {
           res.end( data );
        });
    });

    app.get('/a', function (req, res) {
        fs.readFile( __dirname + "/../demo/index.html", 'utf8', function (err, data) {
           res.end( data );
        });
    });

}

module.exports = appRouter;